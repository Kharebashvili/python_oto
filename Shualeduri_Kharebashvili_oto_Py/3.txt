def character_frequency(string):
    freq = {}
    
    for char in string:
        if char in freq:
            freq[char] += 1
        else:
            freq[char] = 1
    
    return freq

def write_frequency_table_to_file(freq_table, filename="result.txt"):
    with open(filename, 'w') as file:
        file.write("char frequency:\n")
        for char, count in freq_table.items():
            file.write(f"'{char}': {count}\n")

string = "WE ARE GAU"
freq_table = character_frequency(string)
write_frequency_table_to_file(freq_table)

#1

import math

def calculate_cylinder_volume(radius, height):

    volume = math.pi * radius**2 * height
    settings = {'radius': radius, 'height': height}
    return volume, settings

def main():
    radius = float(input("ჩაწერე ცილინდრის რადიუსი: "))
    height = float(input("ჩაწერე ცილინდრის სიმაღლე: "))

    volume, settings = calculate_cylinder_volume(radius, height)
    
    print("ცილინდრის მოცულობა:", volume)
    print("ცილინდრის პარამეტრები:", settings)

if __name__ == "__main__":
    main()

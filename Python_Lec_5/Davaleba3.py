# 3

def calculate_triangle_area(a, b, c):
    s = (a + b + c) / 2
    area = (s * (s - a) * (s - b) * (s - c)) ** 0.5
    return area

def main():
    a = float(input("ჩაწერე სამკუთხედის პირველი გვერდის სიგრძე: "))
    b = float(input("ჩაწერე სამკუთხედის მეორე გვერდის სიგრძე: "))
    c = float(input("ჩაწერე სამკუთხედის მესამე გვერდის სიგრძე: "))

    area = calculate_triangle_area(a, b, c)
    
    print("სამკუთხედის ფართობია:", area)

if __name__ == "__main__":
    main()

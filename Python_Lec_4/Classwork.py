# 1 --------------------------------------------------------------------------

# Oto_subjects = {"c#", "js", "python", "html"}
# Dato_subjects = {"c#", "sql", "js"}

# tanakveta = Dato_subjects & Oto_subjects
# print("tanakveta:",tanakveta)

# union_set = Oto_subjects.union(Dato_subjects)
# print("Union of sets:", union_set)

# intersection_set = Oto_subjects.intersection(Dato_subjects)
# print("Intersection of sets:", intersection_set)

# Oto_difference = Oto_subjects.difference(Dato_subjects)
# print("Difference (Oto - Dato):", Oto_difference)

# Dato_difference = Dato_subjects.difference(Oto_subjects)
# print("Difference (Dato - Oto):", Dato_difference)


# 2 -------------------------------------------------------------------------

# # Piroba romelic amowmebs piradi nomris cifrebis raodenobas
# def validate_personal_number(personal_number):
#     return personal_number.isdigit() and len(personal_number) == 11

# # Piroba romelic amowmebs saxeli
# def validate_name(name):
#     return name.isalpha()

# # Piroba romelic amowmebs saxeli da gvari ar gameordes
# def check_duplicate_names(student_info, myStudentTuple):
#     for info in myStudentTuple:
#         if (info[1].lower(), info[2].lower()) == (student_info[1].lower(), student_info[2].lower()):
#             return False
#     return True

# # Piroba romelic amowmebs piradi nomeri ar gameordes
# def check_duplicate_personal_number(personal_number, myStudentTuple):
#     for info in myStudentTuple:
#         if personal_number == info[0]:
#             return False
#     return True

# # Piroba romelic amowmebs klaviaturidan sheiyvanos studentma info
# def get_student_info(myStudentTuple):
#     while True:
#         personal_number = input("Chawere Piradi Nomeri (11 Cifrit): ")
#         if validate_personal_number(personal_number) and check_duplicate_personal_number(personal_number, myStudentTuple):
#             break
#         elif not validate_personal_number(personal_number):
#             print("Araswori Piradi Nomeria. Chawere Mxolod 11 Cifrit.")
#         else:
#             print("Piradi Nomeri Ukve Arsebobs. Chawere Gansxvavebuli Piradi Nomeri.")

#     while True:
#         first_name = input("Chawere Saxeli: ")
#         if validate_name(first_name):
#             break
#         else:
#             print("Araswori Saxelia. Chawere Mxolod Asoebit.")

#     while True:
#         last_name = input("Chawere Gvari: ")
#         if validate_name(last_name):
#             break
#         else:
#             print("Araswori Gvaria. Chawere Mxolod Asoebit.")

#     if not check_duplicate_names((personal_number, first_name, last_name), myStudentTuple):
#         print("Saxelis Da Gvaris Kombinacia Ukve Arsebobs. Chawere Gansxvavebuli Saxeli Da Gvari.")
#         return get_student_info(myStudentTuple)

#     return (personal_number, first_name, last_name)

# # Programis gashveba
# def main():
#     num_students = int(input("Chawere Studentebis Raodenoba: "))
#     myStudentTuple = []

#     for i in range(num_students):
#         print(f"\nChawere Studentebis Informacia {i+1}:")
#         student_info = get_student_info(myStudentTuple)
#         myStudentTuple.append(student_info)

#     print("\nStudentebis Informacia Shenaxulia myStudentTuple:")
#     print(myStudentTuple)

# if __name__ == "__main__":
#     main()


# 3 -------------------------------------------------------------------------------------------------------------

def calculate_gpa_average(student):
    return student[3]

def top_three_students(students):
    sorted_students = sorted(students, key=calculate_gpa_average, reverse=True)
    return sorted_students[:3]

def main():
    students = [
        ("12345678901", "oto", "gvari1", 3.8),
        ("23456789012", "dato", "gvari2", 3.9),
        ("34567890123", "vano", "gvari3", 3.7),
        ("45678901234", "gio", "gvari4", 4.0),
        ("56789012345", "levani", "gvari5", 3.95)
    ]

    top_students = top_three_students(students)

    print("Top 3 Studenti:")
    for i, student in enumerate(top_students, start=1):
        print(f"{i}. {student[1]} {student[2]} - GPA: {student[3]}")

if __name__ == "__main__":
    main()

#2

def calculate_triangle_area(base, height):
    area = 0.5 * base * height
    return area

def main():
    base = float(input("ჩაწერე სამკუთხედის გვერდის სიგრძე(რომელზეც დაშვებულია სიმაღლე): "))
    height = float(input("ჩაწერე სამკუთხედის სიმაღლის სიგრძე: "))
    area = calculate_triangle_area(base, height)    
    print("სამკუთხედის ფართობია:", area)

if __name__ == "__main__":
    main()

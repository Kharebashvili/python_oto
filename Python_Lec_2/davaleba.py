# 1


# def generate_tickets():
#     for ticket in range(100000, 1000000):
#         first_half = ticket // 1000
#         second_half = ticket % 1000
#         if sum(map(int, str(first_half))) == sum(map(int, str(second_half))):
#             print(ticket)

# generate_tickets()



# 2

# def is_palindrome(text):

#     text = text.replace(" ", "").lower()
#     return text == text[::-1]

# def main():
#     user_input = input("ჩაწერე სიტყვა ან წინადადება რათა შევამოწმოთ პალინდრომია თუ არა: ")
#     if is_palindrome(user_input):
#         print("დიახ, ეს არის პალინდრომი!")
#     else:
#         print("არა, ეს არ არის პალინდრომი.")

# if __name__ == "__main__":
#     main()


# 3

# def are_anagrams(str1, str2):
#     str1 = str1.replace(" ", "").lower()
#     str2 = str2.replace(" ", "").lower()
    
#     return sorted(str1) == sorted(str2)

# def main():
#     line1 = input("ჩაწერე პირველი სიტყვა: ")
#     line2 = input("ჩაწერე მეორე სიტყვა: ")
    
#     if are_anagrams(line1, line2):
#         print("დიახ, ეს სიტყვები არის ანაგრამები!")
#     else:
#         print("არა, ეს სიტყვები არ არის ანაგრამები.")

# if __name__ == "__main__":
#     main()


# 4 გადაყვანა ორობითიდან ათობითში (8 ციფრით)

def binary_to_decimal(binary_string):
    decimal_number = int(binary_string, 2)
    return decimal_number

def main():
    binary_string = input("ჩაწერე 8 ციფრი ორობითში ( მხოლოდ 0 ან 1): ")
    if len(binary_string) != 8 or not all(char in '01' for char in binary_string):
        print("არასწორია! გთხოვთ ჩაწერეთ 8 ციფრი,რომელიც მხოლოდ 0-ებს და 1-ებს შეიცავს!.")
    else:
        decimal_number = binary_to_decimal(binary_string)
        print("ათობითი ჩანაწერი:", decimal_number)

if __name__ == "__main__":
    main()

name = "Oto vaarrr"

# print(name.rindex("is"))
# print(name.rfind("is"))


print(name.count("is",10))
print(name.capitalize())
print(name.title())
print(name.upper())
print(name.lower())
print(name.swapcase())
print(name.startswith("g"))
print(name.endswith("."))
print(name.islower())
print(name.isupper())
print(name.isalpha())
print(name.isalnum())
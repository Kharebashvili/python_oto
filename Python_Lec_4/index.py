# set1= {1,2,43,54,57,2,1,5}
# print(set1,type(set1))


# dict1 = {"Name":"Giorgi","Surname":"Tchelidze","Age":18}
# print(dict1,type(dict1))
# print(dict1["Surname"])
# dict1["Age"]=17
# print(dict1)
# dict1["Mobile"]="571329966"
# print(dict1)
# del dict1["Mobile"]
# print(dict1)

# del dict1
# print(dict1)

subject_tuple = ("math","c#","sql")
giorgi_dict = dict.fromkeys(subject_tuple, 0)
giorgi_dict["math"] = 100
giorgi_dict["c#"] = 53
giorgi_dict["sql"] = 71
print(giorgi_dict)

# print(giorgi_dict.items())
for key, value in giorgi_dict.items():
    print(f'Subject:{key};\nScore:{value}point;\n')

print(tuple(giorgi_dict.keys()))

avg = sum((list(giorgi_dict.values()))/len(giorgi_dict))
print(avg)